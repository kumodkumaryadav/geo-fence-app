import 'dart:async';

import 'package:easy_geofencing/easy_geofencing.dart';
import 'package:easy_geofencing/enums/geofence_status.dart';
import 'package:flutter/material.dart';
import 'package:geo_fence_demo/geo_fence_service.dart';
import 'package:geo_fence_demo/geo_locator.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (BuildContext context) =>GeofenceStatusModel() ,
      child: const MaterialApp(
        
        
        home: HomePage(),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
        title: Text("Geo Fence demo"),
      ),
      body: BodyContent(),
    );
  }
}

class BodyContent extends StatefulWidget {
  const BodyContent({super.key});

  @override
  State<BodyContent> createState() => _BodyContentState();
  
}

class _BodyContentState extends State<BodyContent> {
var position;
late StreamSubscription<GeofenceStatus> geoFenceStream;
 GeofenceStatus? previousStatus;

  @override
  void initState() {
   GeoLocatorService.determinePosition().then((value) {
position=value;
EasyGeofencing.startGeofenceService(pointedLatitude: "22.5502141", pointedLongitude: "88.3496937", radiusMeter: '100', eventPeriodInSeconds: 30);
geoFenceStream=EasyGeofencing.getGeofenceStream()!.listen((GeofenceStatus status) {
  context.read<GeofenceStatusModel>().updateGeofenceStatus(status);
  
  

 if(previousStatus !=status) {
   showStatusSnackBar(status);

 }
 previousStatus=status;
 });
 @override
  void dispose() {
    // Cancel the subscription when the widget is disposed
    // geoFenceStream.cancel();
    super.dispose();
  }


   });
   print(" pistion data $position");
    // TODO: implement initState
    super.initState();
  }
  void showStatusSnackBar(GeofenceStatus status) {

    if(status.toString().contains('enter')){
       ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('You entered in geo fence area'),
        duration: Duration(seconds: 2),
      ),
    );
    } else if(status.toString().contains('exit')){
       ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('You are outside the geo fence area'),
        duration: Duration(seconds: 2),
      ),
    );
    }
   
  }
  @override
  Widget build(BuildContext context) {
  
    return Center(
      
     child: Consumer<GeofenceStatusModel>(
       builder: (context, authStatus, child) {
         return Text("geo fence status: ${authStatus.geofenceStatus.toString()}");
         
       }
     ),
    );
  }
}