
import 'dart:async';

import 'package:easy_geofencing/easy_geofencing.dart';
import 'package:easy_geofencing/enums/geofence_status.dart';
import 'package:flutter/material.dart';
import 'package:geo_fence_demo/geo_locator.dart';
class GeoFenceProvider extends ChangeNotifier{
 late final GeofenceStatus geofenceStatus;
 
  StreamSubscription<GeofenceStatus> geoFenceStream=EasyGeofencing.getGeofenceStream()!.listen((GeofenceStatus status) { 
 
 
  });

  @override
  notifyListeners();

  
  

 


    }


class GeofenceStatusModel extends ChangeNotifier {
  GeofenceStatus? _geofenceStatus ;

  GeofenceStatus? get geofenceStatus => _geofenceStatus;

  void updateGeofenceStatus(GeofenceStatus status) {
    _geofenceStatus = status;
    notifyListeners();
  }
}






